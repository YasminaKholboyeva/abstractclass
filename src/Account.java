public class Account extends DoTransaction{
    private Long id;
    private Long userId;
    private String account;
    private int startBalance;
    private int balance;
    private String system;

    public Account() {
    }

    public Account(Long id, Long userId, String account, int startBalance, int balance, String system) {
        this.id = id;
        this.userId = userId;
        this.account = account;
        this.startBalance = startBalance;
        this.balance = balance;
        this.system = system;
    }
    @Override
    void doTransaction(Account accountFrom, Account accountTo, int amount, String system){
//        int debitAmount = accountFrom.balance - amount;
//        System.out.println("Debited from your account: " + debitAmount);
        int balanceFrom = accountFrom.getStartBalance();
        System.out.println("Your actual balance " + balanceFrom);

        int debitFrom = accountFrom.getBalance() - amount;
        accountFrom.setBalance(debitFrom);

        System.out.println("Result of debit from balance " + accountTo.getBalance());
        accountTo.setBalance(accountTo.getBalance() + amount);


        int operationCredit = accountTo.balance + amount;
        System.out.println("Balance " + accountTo + "successfully replenished ");


    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getStartBalance() {
        return startBalance;
    }

    public void setStartBalance(int startBalance) {
        this.startBalance = startBalance;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", userId=" + userId +
                ", account='" + account + '\'' +
                ", startBalance=" + startBalance +
                ", balance=" + balance +
                ", system='" + system + '\'' +
                '}';
    }
}
